﻿using System;
using CleanArchitecture.Core.Models;
using FluentAssertions;
using NUnit.Framework;

namespace CleanArchitecture.Core.Tests.Models
{
    [TestFixture]
    public class StudentTests
    {
        [Test,Category("Unit")]
        public void Create_NotEmptyName_Success()
        {
            // Arrange
            const string c_name = "Not empty name";

            // Act
            var student = new Student(c_name);

            // Assert
            student.Should().NotBeNull();
            student.Name.Should().Be(c_name);
        }

        [Test, Category("Unit")]
        [TestCase("")]
        [TestCase(null)]
        public void Create_EmptyName_Failure(string name)
        {
            // Arrange
            Func<Student> createStudentAction = () => new Student(name);

            // Act & Assert
            createStudentAction.Should().Throw<ArgumentNullException>();
        }
    }
}