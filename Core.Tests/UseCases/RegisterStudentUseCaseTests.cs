﻿using System;
using CleanArchitecture.Core.DataAccess;
using CleanArchitecture.Core.Models;
using CleanArchitecture.Core.UseCases.StudentsManagement.Register;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace CleanArchitecture.Core.Tests.UseCases
{
    [TestFixture]
    public class RegisterStudentUseCaseTests
    {
        [Test, Category("Unit")]
        public void Register_NullInput_Failure()
        {
            // Arrange
            var studentDataAccessMock = new Mock<IStudentDataAccess>();
            var registerUseCase = new RegisterStudentUseCase(studentDataAccessMock.Object);

            // Act
            var response = registerUseCase.Execute(null);

            // Assert
            response.Should().NotBeNull();
            response.Success.Should().BeFalse();
        }

        [Test, Category("Unit")]
        public void Register_NewStudent_Success()
        {
            // Arrange
            var student = new Student("New student");
            var registerRequest = new RegisterStudentRequest(student);

            var studentDataAccessMock = new Mock<IStudentDataAccess>();
            studentDataAccessMock
                .Setup(x => x.Create(student))
                .Returns(true);
            studentDataAccessMock
                .Setup(x => x.Exists(student.Id))
                .Returns(true);

            var registerUseCase = new RegisterStudentUseCase(studentDataAccessMock.Object);

            // Act
            var response = registerUseCase.Execute(registerRequest);

            // Assert
            response.Should().NotBeNull();
            response.Success.Should().BeTrue();
            studentDataAccessMock
                .Verify(x => x.Create(student),
                    Times.Once);
        }

        [Test, Category("Unit")]
        public void Register_IdAlreadyExists_Failure()
        {
            // Arrange
            var student = new Student("Already existing student");
            var registerRequest = new RegisterStudentRequest(student);

            var studentDataAccessMock = new Mock<IStudentDataAccess>();
            studentDataAccessMock
                .Setup(x => x.Exists(student.Id))
                .Returns(true);

            var registerUseCase = new RegisterStudentUseCase(studentDataAccessMock.Object);

            // Act
            var response = registerUseCase.Execute(registerRequest);

            // Assert
            response.Should().NotBeNull();
            response.Success.Should().BeFalse();
        }

        // Update_StudentExists_Success
        // Update_StudentDoesntExist_Failure

        // GetAllStudents_StudentsExists_Success
        // GetAllStudents_NoStudentExist_Success

        // GetStudentById_StudentExists_Success
        // GetStudentById_StudentDoesntExist_Failure

        // DeleteStudent_StudentExists_Success
        // DeleteStudent_StudentDoesntExist_Failure
    }
}