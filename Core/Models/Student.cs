﻿using System;

namespace CleanArchitecture.Core.Models
{
    public class Student
    {
        public Guid Id { get; internal set; }

        public string Name { get; internal set; }

        public Student(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
            Id = Guid.NewGuid();
        }
    }
}