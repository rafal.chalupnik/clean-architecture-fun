﻿using System;
using CleanArchitecture.Core.Models;

namespace CleanArchitecture.Core.UseCases.StudentsManagement.Register
{
    public class RegisterStudentRequest
    {
        public Student Student { get; }

        public RegisterStudentRequest(Student student)
        {
            Student = student ?? throw new ArgumentNullException(nameof(Student));
        }
    }
}