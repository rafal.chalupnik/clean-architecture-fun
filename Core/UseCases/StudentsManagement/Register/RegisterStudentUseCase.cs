﻿using CleanArchitecture.Core.DataAccess;

namespace CleanArchitecture.Core.UseCases.StudentsManagement.Register
{
    public class RegisterStudentUseCase : IUseCase<RegisterStudentRequest, RegisterStudentResponse>
    {
        private readonly IStudentDataAccess m_studentDataAccess;

        public RegisterStudentUseCase(IStudentDataAccess studentDataAccess)
        {
            m_studentDataAccess = studentDataAccess;
        }

        public RegisterStudentResponse Execute(RegisterStudentRequest input)
        {
            if (input == null)
                return new RegisterStudentResponse(success: false);

            throw new System.NotImplementedException();
        }
    }
}