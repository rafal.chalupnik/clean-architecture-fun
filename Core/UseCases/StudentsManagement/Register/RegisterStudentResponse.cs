﻿namespace CleanArchitecture.Core.UseCases.StudentsManagement.Register
{
    public class RegisterStudentResponse
    {
        public bool Success { get; }

        public RegisterStudentResponse(bool success)
        {
            Success = success;
        }
    }
}