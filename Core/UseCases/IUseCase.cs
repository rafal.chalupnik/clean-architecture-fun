﻿namespace CleanArchitecture.Core.UseCases
{
    public interface IUseCase<in TInput, out TOutput>
    {
        TOutput Execute(TInput input);
    }
}