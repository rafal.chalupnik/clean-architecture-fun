﻿using System;
using CleanArchitecture.Core.Models;

namespace CleanArchitecture.Core.DataAccess
{
    public interface IStudentDataAccess
    {
        bool Create(Student student);

        bool Exists(Guid student);
    }
}